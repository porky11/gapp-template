pub trait Convert {
    type Output;
    fn convert(&self) -> Self::Output;
}

impl Convert for simple_color::Color {
    type Output = femtovg::Color;
    fn convert(&self) -> Self::Output {
        femtovg::Color::rgba(self.r, self.g, self.b, self.a)
    }
}
