use crate::convert::Convert as _;

use femtovg::{Canvas, Renderer};
use gapp::{Render, Update};
use gapp_winit::WindowInput;
use simple_color::Color;
use winit::{
    event::{ElementState, ModifiersState, WindowEvent},
    event_loop::ControlFlow,
};

pub struct Editor {
    cursor: (f32, f32),
    size: (f32, f32),
}

impl Editor {
    pub fn new() -> Self {
        Self {
            cursor: (0.0, 0.0),
            size: (0.0, 0.0),
        }
    }
}

impl Update for Editor {
    fn update(&mut self, _timestep: f32) {}
}

impl<R: Renderer> Render<Canvas<R>> for Editor {
    fn render(&self, canvas: &mut Canvas<R>) {
        canvas.clear_rect(
            0,
            0,
            canvas.width(),
            canvas.height(),
            Color::gray(128).convert(),
        );

        canvas.flush();
    }
}

impl<R: Renderer> WindowInput<ModifiersState, Canvas<R>> for Editor {
    fn input(
        &mut self,
        event: &WindowEvent<'_>,
        control_flow: &mut ControlFlow,
        context_modifiers: &mut ModifiersState,
        canvas: &mut Canvas<R>,
    ) {
        match event {
            &WindowEvent::ModifiersChanged(modifiers) => *context_modifiers = modifiers,

            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,

            WindowEvent::Resized(size) => {
                let (w, h) = (size.width, size.height);
                self.size = (w as f32, h as f32);
                canvas.set_size(w, h, 1.0);
                canvas.reset_transform();
                canvas.translate(w as f32 / 2.0, h as f32 / 2.0);
            }

            WindowEvent::CursorMoved { position, .. } => {
                let (x, y) = (
                    position.x as f32 - self.size.0 / 2.0,
                    position.y as f32 - self.size.1 / 2.0,
                );
                self.cursor = (x, y);
            }

            WindowEvent::MouseInput { state, .. } => {
                use ElementState::*;
                match state {
                    Pressed => (),
                    Released => (),
                }
            }

            _ => (),
        }
    }
}
